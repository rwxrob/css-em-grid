
var c = require('./config');
process.env.LIVERELOAD = true;

var htmld = c.clientd,
    stylesd = c.clientd + '/styles',
    gulp = require("gulp"),
    glob = require("glob"),
    fs = require("fs"),
    p = require("gulp-load-plugins")();

var paths = {
  stylus: [
    // broken in gulp-stylus and reported
    //c.srcClient + '/**/*.styl'
    c.srcClient + '/styles/main.styl'
  ],
  stylusSrc: [
    //c.srcClient + '/styles/**/*.styl',
    c.srcClient + '/styles/main.styl',
    '!' + c.srcClient + '/styles/**/*~'
  ],
  stylesAsis: [
    c.srcClient + '/styles/**/*',
    '!' + c.srcClient + '/styles/**/*{~,.styl}'
  ],
  clean: [
    c.clientd + '/*.html', 
    c.clientd + '/styles'
  ]
};

gulp.task('clean',function(){
  return gulp.src(paths.clean, {read: false})
    .pipe(p.clean());
});

//------------------------------- styles ---------------------------------

gulp.task('styles-stylus', function(){
  return gulp.src(paths.stylusSrc)
    .pipe(p.stylus({path: paths.stylus, errors: true}))
    //.pipe(p.autoprefixer('last 2 versions'))
    .pipe(gulp.dest(stylesd))
    .pipe(p.minifyCss({keepSpecialComments: 0}))
    .pipe(p.rename({extname: ".min.css"}))
    .pipe(gulp.dest(stylesd));
});

gulp.task('styles-asis', function(){
  return gulp.src(paths.stylesAsis)
    .pipe(gulp.dest(stylesd));
});

gulp.task('styles',['styles-stylus','styles-asis']);

//------------------------------- html -----------------------------------

gulp.task('html-jade', function(){
  return gulp.src(c.srcClient + '/html/**/*.jade')           
    // don't try pretty: true here or you'll mess up the grid with
    // the added spaces
    .pipe(p.jade())
    .pipe(gulp.dest(htmld));
});

gulp.task('html', ['html-jade']);

//------------------------------- watch ----------------------------------

gulp.task('watch', function(){
  p.nodemon({script: 'server.js', execMap: {js: "node --harmony"}});
  var lr = p.livereload();
  var u = function(file) {lr.changed(file.path);};
  gulp.watch(c.srcClient + '/styles/**/*', ['styles']).on('change',u);
  gulp.watch(c.srcClient + '/html/**/*', ['html']).on('change',u);
});

//------------------------------- default --------------------------------

gulp.task('once',['styles','html']);
gulp.task('default',['once','watch']);
