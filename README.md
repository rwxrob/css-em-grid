css-em-grid
===========

EM grid is a pure HTML5/CSS3 tool to help with mobile-first,
device-agnostic, responsive development workflows. Stylus/Jade includes
and HTML partial versions are available. It's all on
[GitHub](https://github.com/robmuh/css-em-grid) and NPM.


 `npm install css-em-grid --save-dev`

For now you can just grab or link to 

 `node_modules/css-em-grid/em-grid.html`
 `node_modules/css-em-grid/styles/main.css`
 `node_modules/css-em-grid/styles/main.min.css`

Or you can mess around with stuff in `src/client` until we can bake
the package a bit more:

 `node_modules/css-em-grid/src/client/html/em-grid.jade`
 `node_modules/css-em-grid/src/client/styles/em-grid.styl`


Why?
====

First of all, you hate grid frameworks like Foundation and Bootstrap. They
add unnecesary weight and syntax to remember. If you ever do need
a grid, you build one for your needs in a few lines.

You are a true modern, mobile-first, device-agnostic web developer who
always starts with something that looks great on anything 20em and smaller
(320px if you haven't messed with your base font, which you haven't 'cuz
you're smart) and work your way up through media queries while resizing
without a care to any device-specific pixel width. You give yourself about
1em to play with and notice you need granularity in different widths as
you get larger. You love EMs and percentages and size *everything* in them
so your responsive site is bullet-proof against even users zooming in or
out in their browsers. In fact, you actually *like* the relative nature of
the `em` font-sizing because you've organized your content and styles so
you expect all your fonts to resize based on one base-font `em` change,
after all, that's why it was created that way.

Your eyes aren't so good even though you're not very old and you hate
squinting at `width` using whatever tool you use during your resizing
sessions. You prefer not to make a half-dozen extra clicks to even
see the viewport size when you do. You probably have livereload
working like a champ and prefer to use the mouse as little as
possible.

But most of all, you hate that webkit browsers such as Chrome do not count
the 17 or so pixels in your viewport's right edge which means you never
see those 17 pixels because you are buildng SPAs that always trigger
the scrollbar and since 17 pixels is about 1em, your responsive
adjustment buffer, this pisses you off enough find or make something
to get around it.

Welcome.

You just might like this visual guide through the perils and power of em
sizing that you don't even have to think about much.

Nothing fancy, no JavaScript, no extensions to load in your browser, etc. Just
add the `position fixed` em grid to your page during workflow, adjust
to taste with the `#em-grid` selector and you'll be seeing rainbows of
ems in no time.

Then again, you could just use a Wordpress template.
