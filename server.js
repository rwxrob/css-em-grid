
var config = require('./config');

var koa = require('koa');
var app = module.exports = koa();

// rudimentary logging and other common stuff
var common = require('koa-common');
app.use(common.logger());
app.use(common.responseTime());

// livereload - only from gulp
if (Boolean(process.env.LIVERELOAD)) {
  var livereload = require('koa-livereload');
  app.use(livereload());
}

// client-side stuff
app.use(common.static(config.clientd));

// great things happen to those that ...
app.listen(config.port);
console.log('listening on *:' + config.port);
